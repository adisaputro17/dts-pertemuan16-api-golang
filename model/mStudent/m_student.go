package mStudent

import (
	"database/sql"
	"time"
	"log"
)

type Student struct {
	Id int64 
	Nim string // `json:"nim"`
	Name string
	Semester int
	CreatedAt time.Time
	UpdatedAt time.Time
}

const dateFormat = `2006-01-02 15:04:05`

func SelectAll(db *sql.DB) (students []Student, err error) {
	rows, err := db.Query(`SELECT * FROM students ORDER BY id DESC`)
	students = []Student{}
	if err != nil {
		return students, err
	}
	defer rows.Close()
	for rows.Next() {
		s := Student{}
		createdAt, updatedAt := ``, ``
		err = rows.Scan(
			&s.Id,
			&s.Nim,
			&s.Name,
			&s.Semester,
			&createdAt,
			&updatedAt)
		if err != nil {
			return 
		}
		s.CreatedAt, _ = time.Parse(dateFormat, createdAt)
		s.UpdatedAt, _ = time.Parse(dateFormat, updatedAt)
		students = append(students, s)
	}
	return
}

func Insert(db *sql.DB, m *Student) (err error) {
	now := time.Now()
	res, err := db.Exec(`INSERT INTO students(name,nim,semester,created_at,updated_at)
VALUES(?,?,?,?,?)`,
	m.Name,
	m.Nim,
	m.Semester,
	now,
	now)
	if err !=nil {
		return err
	}
	m.Id, err = res.LastInsertId()
	if err == nil {
		m.CreatedAt = now
		m.UpdatedAt = now
	}
	return err
}

func Update(db *sql.DB, m *Student) (number int64) {
	var student Student
	now := time.Now()
	err := db.QueryRow("select nim, name, semester from students where id = ?", m.Id).
		Scan(&student.Nim, &student.Name, &student.Semester)
	if err !=nil {
		log.Println(err.Error())
	}
	if m.Semester > 0 {
		student.Semester = m.Semester
	}

	if m.Name != `` {
		student.Name = m.Name
	}

	if m.Nim != `` {
		student.Nim = m.Nim
	}

	res, err := db.Exec(`UPDATE students set nim = ?, name = ?, semester = ?, updated_at = ? where id = ?`,
		student.Nim,
		student.Name,
		student.Semester,
		now,
		m.Id,
	)
	
	if err != nil {
		log.Println(err.Error())
	}
	
	count, err2 := res.RowsAffected()
	
	if err2 != nil {
		log.Println(err2.Error())
	} else {
		log.Println(count)
	}
	return count
}
// Adhi Dwi Saputro

func Delete(db *sql.DB, m *Student) bool {
	res, err := db.Exec(`DELETE from students WHERE id = ?`, m.Id)

	if err != nil {
		log.Println(err.Error())
	}
	
	count, err2 := res.RowsAffected()
	
	if err2 != nil {
		log.Println(err2.Error())
	} else {
		log.Println(count)
	}
	
	if count > 0{
		return true
	}else{
		return false
	}
}
// Adhi Dwi Saputro